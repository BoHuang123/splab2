#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "readfile_v2.h"
int order(const void *a, const void *b){
    int d1=((struct Employee *)a)->DigitID;
    int d2=((struct Employee *)b)->DigitID;
    return d1>d2 ? 1:-1;
}

int main (int argc,char *argv[]){
//int main (){
    int Choice,DigitID,Salary,EmployeeNumber,i,testflag,testflag2,testflag3,testflag4,j,TopM=0;
    int Employlist[MAXNUM];
    char FirstName[MAXNUM],LastName[MAXNUM],InFileName[MAXNUM],Message,tmpname,Decision[10];
    FILE *fp;
    struct Employee TmpEmployee[MAXNUM];
    testflag=0;
    testflag2=0;
    testflag3=0;
    tmpname=" ";

    i=0;
    printf("%s\n",argv[1]);
    /*printf("input file:\n");
    scanf("%s", InFileName);
    printf("your input is: %s\n",InFileName);
*/
     if(open_file(argv[1])== -1 ){
     //if(open_file(InFileName)== -1 ){
        printf("Fail to open file!\n");
        exit(0);
    }
    else {
        fp=fopen(argv[1],"r");
       // printf("%s will be used",&InFileName);
       // fp=fopen(InFileName,"r");
    }

    //printf("\ncurrent list is: ");
    while(!feof(fp)){
        fscanf(fp,"%d  %s  %s  %d\n", &TmpEmployee[i].DigitID , &TmpEmployee[i].FirstName, &TmpEmployee[i].LastName, &TmpEmployee[i].Salary);
        //printf("\n%d,%s,%s",TmpEmployee[i].DigitID,&TmpEmployee[i].FirstName,&TmpEmployee[i].LastName);
        i++;
    }

    EmployeeNumber=i;
    //fclose(fp);
    close_file(fp);

    //printf("\nnumber of Employee is %d",EmployeeNumber);
    /*qsort(TmpEmployee,EmployeeNumber,sizeof(struct Employee),order);

    for (i=0; i<EmployeeNumber;i++){
        Employlist[i]=TmpEmployee[i].DigitID;
        //printf("\n%d",Employlist[i]);
    }*/
    //Message="Employee DB Menu:\n----------------------------------\n  (1) Print the Database\n  (2) Lookup by ID\n  (3) Lookup by Last Name\n  (4) Add an Employee\n  (5) Quit\n----------------------------------\nEnter your choice:";
    //printf("%s",&Message);
    while(1){
    qsort(TmpEmployee,EmployeeNumber,sizeof(struct Employee),order);
    for (i=0; i<EmployeeNumber;i++){
        Employlist[i]=TmpEmployee[i].DigitID;
    }
    printManu();
    testflag3=0;
    testflag2=0;
    testflag=0;
        while(1){
        read_int(&Choice);
        if (Choice<10 && Choice>0)
        {
            testflag2=1;
        }
        else{
            printf("Hey, %d is not between 1 and 9, try again...\n",Choice);
            printf("Enter your choice:");
            testflag2=0;
            continue;

        }
        if (testflag2==1)
        {
            break;
        }
        else{
            continue;
        }
        }


    switch(Choice){
    case 1:
        {
            printf("NAME                              SALARY 	     ID\n");
            printf("---------------------------------------------------------------\n");
            for (i=0; i<EmployeeNumber;i++){
            printf("%-10s %-10s             %-10d        %-10d\n" , &TmpEmployee[i].FirstName, &TmpEmployee[i].LastName, TmpEmployee[i].Salary,TmpEmployee[i].DigitID);
            }
            printf(" Number of Employees (%d)",EmployeeNumber );
            break;
        }


    case 2:
        {
        printf("now we have %d employee(s)",EmployeeNumber);
        printf("\nEnter a 6 digit employee id:");
        read_int(&DigitID);
        for (i=0; i<EmployeeNumber;i++)
            {
                Employlist[i]=TmpEmployee[i].DigitID;
             }
        i=searchBinary(Employlist,DigitID,EmployeeNumber);
        if (i==-1) break;
        else{
        printf("\nNAME                              SALARY 	     ID\n");
        printf("---------------------------------------------------------------\n");
        printf("%-10s %-10s             %-10d        %-10d\n" , &TmpEmployee[i].FirstName, &TmpEmployee[i].LastName, TmpEmployee[i].Salary,TmpEmployee[i].DigitID);
        printf("---------------------------------------------------------------\n");
        break;
            }
        }
    case 3:
        {
    printf("Enter Employee's last name (no extra spaces): ");
    read_string(LastName);
    for (i=0;i<EmployeeNumber;i++)
    {

        if (strcmp(LastName,TmpEmployee[i].LastName)==0)
        {
            printf("\nNAME                              SALARY 	     ID\n");
            printf("---------------------------------------------------------------\n");
            printf("%-10s %-10s             %-10d        %-10d\n" , TmpEmployee[i].FirstName, TmpEmployee[i].LastName, TmpEmployee[i].Salary,TmpEmployee[i].DigitID);
            printf("---------------------------------------------------------------\n");
            testflag3=1;
            break;
        }
    }
        if (testflag3==0)
        {
            printf("no matches in the database");
        }
    //printf("%s",&LastName);
    break;
        }
    case 4:
        {
            printf("Enter the first name of the employee:");
            read_string(FirstName);
            //strcpy(&tmpname,&FirstName);
            //printf("FirstName is %s\n",&FirstName);
            printf("Enter the last name of the employee:");
            read_string(LastName);
            //scanf("%s",LastName);
            //printf("FirstName is %s\n",tmpname);
            printf("Enter employee's salary (30000 to 150000): ");
            testflag4=0;
            while(1)
            {
                if (testflag4==1)
                {
                    break;
                }
                read_int(&Salary);
                if (Salary>=30000 && Salary<=150000)
                {
                    testflag4=1;
                }
                else
                {
                    printf("input salary again:");
                }

            }

            printf("do you want to add the following employee to the DB?\n");
            printf("     %s %s,salary:%d\n",FirstName,LastName,Salary);
            printf("Enter 1 for yes,0 for no:");
            read_int(&Choice);
            if (Choice==1)
            {
                TmpEmployee[EmployeeNumber].DigitID=TmpEmployee[EmployeeNumber-1].DigitID+1;
                strcpy(TmpEmployee[EmployeeNumber].FirstName,FirstName);
                strcpy(TmpEmployee[EmployeeNumber].LastName,LastName);
                TmpEmployee[EmployeeNumber].Salary=Salary;
                EmployeeNumber+=1;

            }
            else
            {
                continue;
            }

            break;

        }
    case 5:
        {
            testflag=1;
            break;
        }

    case 6:
        {
            printf("Enter the 6-digit ID of the employee to be removed:");
            read_int(&DigitID);
            i=-1;
            i=searchBinary(Employlist,DigitID,EmployeeNumber);
            if (i==-1) break;
            else{
                printf("\nNAME                              SALARY 	     ID\n");
                printf("---------------------------------------------------------------\n");
                printf("%-10s %-10s             %-10d        %-10d\n" , &TmpEmployee[i].FirstName, &TmpEmployee[i].LastName, TmpEmployee[i].Salary,TmpEmployee[i].DigitID);
                printf("---------------------------------------------------------------\n");
                }
            printf("Do you really want to remove the information of this employee from the database(Y/N):");
            read_string(Decision);


            if (strcmp(Decision,"Y")==0)
            {
                for (i=i;i<EmployeeNumber-1;i++)
                {
                    TmpEmployee[i].DigitID=TmpEmployee[i+1].DigitID;
                    strcpy(TmpEmployee[i].FirstName,TmpEmployee[i+1].FirstName);
                    strcpy(TmpEmployee[i].LastName,TmpEmployee[i+1].LastName);
                    TmpEmployee[i].Salary=TmpEmployee[i+1].Salary;
                }
                EmployeeNumber-=1;
                printf("Now we have %d employee in the database\n\n",EmployeeNumber);
                break;
            }
        break;

        }
    case 7:
        {
            printf("Enter the 6-digit ID of the employee to be updated:");
            read_int(&DigitID);
            i=-1;
            i=searchBinary(Employlist,DigitID,EmployeeNumber);
            if (i==-1) break;
            else{
                printf("\nNAME                              SALARY 	     ID\n");
                printf("---------------------------------------------------------------\n");
                printf("%-10s %-10s             %-10d        %-10d\n" , &TmpEmployee[i].FirstName, &TmpEmployee[i].LastName, TmpEmployee[i].Salary,TmpEmployee[i].DigitID);
                printf("---------------------------------------------------------------\n");
                }
        testflag3=0;

        while(1)
        {
        if(testflag3==1)
        {
            break;
        }
        printf("Which filed do you want to change:\n   1. First Name\n   2. Last Name\n   3. First name and last name\n   4. ID (6-digit)\n   5. Salary\n   6. quit\n");

        while(1){

        read_int(&Choice);
        if (Choice<7 && Choice>0)
        {
            testflag2=1;
        }
        else{
            printf("Hey, %d is not between 1 and 6, try again...\n",Choice);
            printf("Enter your choice:");
            testflag2=0;
            continue;

        }
        if (testflag2==1)
        {
            break;
        }
        else{
            continue;
        }
        }
        switch(Choice)
        {
        case 1:
            {
                printf("enter the NEW first name:");
                read_string(FirstName);
                strcpy(TmpEmployee[i].FirstName,FirstName);
                break;
            }
        case 2:
            {
                printf("enter the NEW last name:");
                read_string(LastName);
                strcpy(TmpEmployee[i].LastName,LastName);
                break;
            }
        case 3:
            {
                printf("enter the NEW first name:");
                read_string(FirstName);
                strcpy(TmpEmployee[i].FirstName,FirstName);
                printf("enter the NEW last name:");
                read_string(LastName);
                strcpy(TmpEmployee[i].LastName,LastName);
                break;
            }
        case 4:
            {
                printf("enter the NEW ID:");
                read_int(&DigitID);
                TmpEmployee[i].DigitID=DigitID;
                break;
            }
        case 5:
            {
                printf("enter the NEW salary:");
                testflag4=0;
            while(1)
            {
                if (testflag4==1)
                {
                    break;
                }
                read_int(&Salary);
                if (Salary>=30000 && Salary<=150000)
                {
                    testflag4=1;
                }
                else
                {
                    printf("input salary again:");
                }

            }
                TmpEmployee[i].Salary=Salary;
                break;
            }
        case 6:
            {
                testflag3=1;
                break;
            }

        default:
            {
                break;
            }
        }
        break;
        }
    break;
        }
    case 8:
        {
            printf("M=");
            testflag2=0;
            while(1)
            {
                if(testflag2==1)
                {
                    break;
                }
                read_int(&TopM);
                if(TopM<0 || TopM>EmployeeNumber)
                {
                    printf("In total we have %d employees,",EmployeeNumber);
                    printf("input M again:");
                    continue;
                }
                else
                {
                    testflag2=1;
                }
            }
            qsort(TmpEmployee,EmployeeNumber,sizeof(struct Employee),cmpSalary);
            printf("\nNAME                              SALARY 	     ID\n");
            printf("---------------------------------------------------------------\n");
            for (i=0;i<TopM;i++)
            {
                printf("%-10s %-10s             %-10d        %-10d\n" , &TmpEmployee[i].FirstName, &TmpEmployee[i].LastName, TmpEmployee[i].Salary,TmpEmployee[i].DigitID);
            }
                printf("---------------------------------------------------------------\n");

            break;

            }
    case 9:
        {
    printf("Enter Employee's last name (no extra spaces): ");
    read_string(LastName);
    testflag4=0;
    testflag3=0;
    testflag2=0;
    for (i=0;i<EmployeeNumber+1;i++)
    {
        if(testflag3==1 && testflag4==0)
        {
            printf("\nNAME                              SALARY 	     ID\n");
            printf("---------------------------------------------------------------\n");
            testflag4=1;
        }
        if(testflag3==1 && testflag2==1)
        {
            i=i-1;
            printf("%-10s %-10s             %-10d        %-10d\n" , TmpEmployee[i].FirstName, TmpEmployee[i].LastName, TmpEmployee[i].Salary,TmpEmployee[i].DigitID);
            i=i+1;

        }
        if (i<EmployeeNumber && strcasecmp(LastName,TmpEmployee[i].LastName)==0 )
        {
            testflag3=1;
            testflag2=1;
        }
        else
        {
            testflag2=0;
        }

    }
        if (testflag3==0)
        {
            printf("no matches in the database");
        }
        else
        {
            printf("---------------------------------------------------------------\n");
        }
    //printf("%s",&LastName);
    break;

        }
    default:printManu();
    }



    if (testflag==1)
    {
        break;
    }
    else
    {
        continue;
    }
    }
}

