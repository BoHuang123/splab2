#define MAXNAME 64
#define MAXNUM 1024
struct Employee{
        char FirstName[MAXNAME];
        char LastName[MAXNAME];
        int DigitID,Salary;
};
int open_file(char *P1){
    if (fopen(P1,"r")==NULL)
        return -1;
    else
        return 0;
}
void close_file(FILE *P1){
  fclose(P1);
}
int read_int(int *address){
    if(scanf("%d", address) == 0)
        return -1;
    else
        return 0;
}
int read_char(int *address){
    if(scanf("%c", address) == 0)
        return -1;
    else
        return 0;
}
int read_string(int *address){
    if(scanf("%s", address) == 0)
        return -1;
    else
        return 0;
}

int read_float(int *address){
    if(scanf("%f", address) == 0)
        return -1;
    else
        return 0;
}
void printManu(){
    printf("\nEmployee DB Menu:\n----------------------------------\n  (1) Print the Database\n  (2) Lookup by ID\n  (3) Lookup by Last Name\n  (4) Add an Employee\n  (5) Quit\n  (6) Remove an employee\n  (7) Update an employee's information \n  (8) Print the M employees with the highest salaries\n  (9) Find all employees with matching last name\n----------------------------------\nEnter your choice:");
}
int searchBinary(int *a,int lookup,int r){
    //printf("%d\n",lookup);
    int i,n;
    for (i=0;i<r;i++){
        //printf("%d, ",a[i]);
    }
	//int a[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	int w = 0;


	int l = 0;
    int m;


	while(1)
	{
		n=m;
		m = (l + r)/2;
		if (n==m)
        {
            break;
        }
		if (lookup == a[m])
		{
			w = 1;
			break;
		}
		else if (lookup > a[m])
		{
			l = m;
		}
		else
		{
			r = m;
		}
	}
	if (w == 0)
	{
		printf("\nID not in the database\n");
	}
    if (w==1){
        return m;
    }
    else{
        return -1;
    }

}
int cmpSalary(const void *p1,const void *p2){
  int s1=((struct Employee *)p1)->Salary;
  int s2=((struct Employee *)p2)->Salary;
  if (s1<s2)
  {
      return 1;
  }
  else
  {
      return -1;
    }

}
